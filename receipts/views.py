from django.shortcuts import render
from receipts.models import Receipt  # ExpenseCategory, Account
# Create your views here.
# all instances of the Receipt model :
def receipts_list(request, id):
    receipt = Receipt.objects.get(id=id)
    list = Receipt.objects.all()
    context = {
        "list": list,
        "receipt": receipt,
    }
    return render(request, "receipts/list.html", context)
